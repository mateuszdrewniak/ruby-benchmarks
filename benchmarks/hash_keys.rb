# frozen_string_literal: true

require_relative '../benchmark_helper'

HASH = Hash[*("a".."zzz").to_a.shuffle]
KEY = "zz"

def key_fast
  HASH.key? KEY
end

def key_slow
  HASH.keys.include? KEY
end

Benchmark.ips do |x|
  x.report("Hash#keys.include?") { key_slow }
  x.report("Hash#key?") { key_fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#   Hash#keys.include?   744.000  i/100ms
#            Hash#key?   775.674k i/100ms
# Calculating -------------------------------------
#   Hash#keys.include?      7.760k (± 3.1%) i/s -     39.432k in   5.086592s
#            Hash#key?      7.776M (± 1.1%) i/s -     39.559M in   5.088091s

# Comparison:
#            Hash#key?:  7775826.4 i/s
#   Hash#keys.include?:     7759.8 i/s - 1002.06x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#   Hash#keys.include?     5.000  i/100ms
#            Hash#key?     3.535M i/100ms
# Calculating -------------------------------------
#   Hash#keys.include?    773.647  (±15.9%) i/s -      3.735k in   4.987823s
#            Hash#key?     35.806M (± 3.0%) i/s -    180.287M in   5.040475s

# Comparison:
#            Hash#key?: 35805703.2 i/s
#   Hash#keys.include?:      773.6 i/s - 46281.72x  (± 0.00) slower
