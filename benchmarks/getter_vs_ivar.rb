# frozen_string_literal: true

require_relative '../benchmark_helper'

class ExampleClass
  attr_accessor :ivar

  def setter
    1000.times do |i|
      self.ivar = i
    end
  end

  def set_ivar
    1000.times do |i|
      @ivar = i
    end
  end
end

Benchmark.ips do |x|
  x.report("setter")   { ExampleClass.new.setter }
  x.report("set @ivar") { ExampleClass.new.set_ivar }
  x.compare!
end

# CRuby 2.6.6
# Comparison:
#            set @ivar:    36459.3 i/s
#               setter:    29170.0 i/s - 1.25x  (± 0.00) slower
