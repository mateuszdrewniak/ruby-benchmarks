# frozen_string_literal: true

require_relative '../benchmark_helper'
require 'date'

DATES = %w[2017-03-01 2017-03-02]

def block
  DATES.map do |date|
    Date.parse(date)
  end
end

def method_to_proc
  DATES.map(&Date.method(:parse))
end

DATE_PARSE = Date.method(:parse).to_proc

def method_converted_to_proc
  DATES.map(&DATE_PARSE)
end

Benchmark.ips do |x|
  x.report('block') { block }
  x.report('Method to Proc') { method_to_proc }
  x.report('Method converted to Proc') { method_converted_to_proc }
  x.compare!
end

# CRuby 2.6.8
# Comparison:
#                block:   264592.0 i/s
#       Method to Proc:   229068.0 i/s - 1.16x  (± 0.00) slower

# CRuby 3.1.1
# Comparison:
#                block:   250626.1 i/s
#       Method to Proc:   229810.9 i/s - 1.09x  (± 0.00) slower
