# frozen_string_literal: true

require_relative '../benchmark_helper'

require "ostruct"

def fast
  { field_1: 1, field_2: 2 }
end

def slow
  OpenStruct.new(field_1: 1, field_2: 2)
end

Benchmark.ips do |x|
  x.report("Hash")       { fast }
  x.report("OpenStruct") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#                 Hash     1.035M i/100ms
#           OpenStruct   158.604k i/100ms
# Calculating -------------------------------------
#                 Hash     10.351M (± 3.3%) i/s -     51.773M in   5.008038s
#           OpenStruct      1.559M (± 8.1%) i/s -      7.772M in   5.043404s

# Comparison:
#                 Hash: 10350609.7 i/s
#           OpenStruct:  1559135.9 i/s - 6.64x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#                 Hash    76.435k i/100ms
#           OpenStruct   261.182M i/100ms
# Calculating -------------------------------------
#                 Hash      2.171B (±19.1%) i/s -     10.104B in   4.869156s
#           OpenStruct      2.625B (± 2.9%) i/s -     13.320B in   5.079648s

# Comparison:
#           OpenStruct: 2624747146.4 i/s
#                 Hash: 2170947015.7 i/s - same-ish: difference falls within error
