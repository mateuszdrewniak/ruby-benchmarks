# frozen_string_literal: true

require_relative '../benchmark_helper'

class MethodCall
  def method
  end

  def method_missing(_method,*args)
    method
  end
end

def fastest
  method = MethodCall.new
  method.method
end

def slow
  method = MethodCall.new
  method.send(:method)
end

def slowest
  method = MethodCall.new
  method.not_exist
end

Benchmark.ips do |x|
  x.report("call")           { fastest }
  x.report("send")           { slow    }
  x.report("method_missing") { slowest }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#                 call   640.201k i/100ms
#                 send   491.735k i/100ms
#       method_missing   401.687k i/100ms
# Calculating -------------------------------------
#                 call      6.326M (± 3.2%) i/s -     32.010M in   5.065406s
#                 send      4.919M (± 2.5%) i/s -     24.587M in   5.001680s
#       method_missing      3.965M (± 2.3%) i/s -     20.084M in   5.068322s

# Comparison:
#                 call:  6326239.2 i/s
#                 send:  4919189.6 i/s - 1.29x  (± 0.00) slower
#       method_missing:  3965020.9 i/s - 1.60x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#                 call    42.519k i/100ms
#                 send   253.192M i/100ms
#       method_missing   248.767M i/100ms
# Calculating -------------------------------------
#                 call      2.218B (±19.5%) i/s -      9.832B in   4.835995s
#                 send      2.448B (± 4.7%) i/s -     12.406B in   5.078962s
#       method_missing      2.467B (± 5.3%) i/s -     12.438B in   5.058664s

# Comparison:
#       method_missing: 2467045559.6 i/s
#                 send: 2448419247.8 i/s - same-ish: difference falls within error
#                 call: 2217614795.5 i/s - same-ish: difference falls within error
