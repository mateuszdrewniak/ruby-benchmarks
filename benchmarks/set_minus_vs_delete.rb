# frozen_string_literal: true

require 'set'

require_relative '../benchmark_helper'

SET = Set.new(0...1_000)
CODE = 5
TIMES = 100_000

def set_minus
  TIMES.times do
    SET - [CODE]
  end
end

def set_delete
  TIMES.times do
    result = SET.dup
    result.delete(CODE)
    result
  end
end

Benchmark.ips do |x|
  x.report('set -') { set_minus }
  x.report('set delete') { set_delete }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#           eval block:  1683641.7 i/s
#          eval string:    13643.8 i/s - 123.40x  (± 0.00) slower
def collection
  ALL.invert.to_a
end
