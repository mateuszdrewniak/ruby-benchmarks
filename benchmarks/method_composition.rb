# frozen_string_literal: true

require_relative '../benchmark_helper'

TIMES = 10_000

def regular_method(val)
  val.to_s.upcase
end

define_method :method_composition, &:to_s.to_proc >> :upcase.to_proc

METHOD_COMPOSITION_PROC = :to_s.to_proc >> :upcase.to_proc

Benchmark.ips do |x|
  x.report('method composition proc') { TIMES.times { METHOD_COMPOSITION_PROC.(:dupa) } }
  x.report('method composition') { TIMES.times { method_composition(:dupa) } }
  x.report('regular method') { TIMES.times { regular_method(:dupa) } }
  x.compare!
end

# CRuby 3.1.0
# Comparison:
#       regular method:      920.7 i/s
#   method composition:      608.2 i/s - 1.51x  (± 0.00) slower
# method composition proc:      602.6 i/s - 1.53x  (± 0.00) slower
