# frozen_string_literal: true

require_relative '../benchmark_helper'

ARRAY = ['PLN', 'pln', 'EUR', 'eur', 'USD', 'UsD']

def to_set
  ARRAY.to_set { _1.upcase }
end

def uniq
  ARRAY.uniq { _1.upcase }
end

Benchmark.ips do |x|
  x.report('to_set') { to_set }
  x.report('uniq') { uniq }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#                 uniq:  1401023.8 i/s
#               to_set:   478521.0 i/s - 2.93x  (± 0.00) slower
