# frozen_string_literal: true

require_relative '../benchmark_helper'

def slow(&block)
  block.call
end

def slow2(&block)
  yield
end

def slow3
  yield if block_given?
end

def fast
  yield
end

Benchmark.ips do |x|
  x.report('block.call') { slow { 1 + 1 } }
  x.report('block + yield') { slow2 { 1 + 1 } }
  x.report('yield if block_given?') { slow3 { 1 + 1 } }
  x.report('yield') { fast { 1 + 1 } }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#           block.call   929.174k i/100ms
#        block + yield     1.015M i/100ms
# yield if block_given?
#                          1.006M i/100ms
#                yield     1.275M i/100ms
# Calculating -------------------------------------
#           block.call      9.319M (± 1.5%) i/s -     47.388M in   5.086105s
#        block + yield     10.125M (± 2.0%) i/s -     50.771M in   5.016709s
# yield if block_given?
#                          10.135M (± 1.4%) i/s -     51.293M in   5.062114s
#                yield     12.716M (± 1.3%) i/s -     63.743M in   5.013666s

# Comparison:
#                yield: 12716190.8 i/s
# yield if block_given?: 10134743.7 i/s - 1.25x  (± 0.00) slower
#        block + yield: 10124905.8 i/s - 1.26x  (± 0.00) slower
#           block.call:  9319387.2 i/s - 1.36x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#           block.call    43.765k i/100ms
#        block + yield   264.372M i/100ms
# yield if block_given?
#                        257.749M i/100ms
#                yield   259.041M i/100ms
# Calculating -------------------------------------
#           block.call      2.047B (±19.7%) i/s -      9.294B in   4.869353s
#        block + yield      2.555B (± 6.1%) i/s -     12.954B in   5.091825s
# yield if block_given?
#                           2.603B (± 2.4%) i/s -     13.145B in   5.052744s
#                yield      2.607B (± 4.9%) i/s -     13.211B in   5.082230s

# Comparison:
#                yield: 2606956091.4 i/s
# yield if block_given?: 2603114057.5 i/s - same-ish: difference falls within error
#        block + yield: 2555183197.0 i/s - same-ish: difference falls within error
#           block.call: 2046666249.7 i/s - 1.27x  (± 0.00) slower
