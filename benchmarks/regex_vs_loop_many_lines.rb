# frozen_string_literal: true

require_relative '../benchmark_helper'

PAYLOAD = <<~STR
-----BEGIN PGP MESSAGE-----
ZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYS
BsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgb
GEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WC
YWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJve
mvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3
prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a
8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmF
kYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgb
GEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhI
GxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYW
R1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIH
RydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSB
zacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb
3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCY
WRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYS
BsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGE
gbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVw
YSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1c
ZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYS
BsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgb
GEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WC
YWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJve
mvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3
prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a
8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmF
kYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgb
GEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhI
GxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYW
R1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIH
RydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSB
zacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb
3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCY
WRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYS
BsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGE
gbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVw
YSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1c
GEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGE=
-----END PGP MESSAGE-----
STR

def while_loops_each_char
  str = PAYLOAD
  first_newline_index = 0

  i = 0
  strlen = str.length
  while i < strlen
    break first_newline_index = i if str[i] == "\n"

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str[i] == "\n"

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

NEWLINE_BYTE = "\n".bytes.first

def while_loops_each_byte
  str = PAYLOAD
  newline_byte = NEWLINE_BYTE
  first_newline_index = 0

  i = 0
  strlen = str.length
  while i < strlen
    break first_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

def lines
  PAYLOAD.lines[1..-2]
end

def each_byte
  str = PAYLOAD
  newline_byte = NEWLINE_BYTE
  first_newline_index = 0

  i = 0
  strlen = str.length

  str.each_byte.with_index do |byte, i|
    break first_newline_index = i if byte == newline_byte

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

def regex
  PAYLOAD[/-----BEGIN PGP MESSAGE-----\n(.*)\n-----END PGP MESSAGE-----/, 1]
end

Benchmark.ips do |x|
  x.report('while loops each char') { while_loops_each_char }
  x.report('while loops each byte') { while_loops_each_byte }
  x.report('each_byte') { each_byte }
  x.report('regex') { regex }
  x.report('lines') { lines }
  x.compare!
end

# Warming up --------------------------------------
# while loops each char
#                         43.950k i/100ms
# while loops each byte
#                         79.951k i/100ms
#            each_byte    32.463k i/100ms
#                regex   165.587k i/100ms
#                lines    39.746k i/100ms
# Calculating -------------------------------------
# while loops each char
#                         424.273k (± 1.9%) i/s -      2.154M in   5.077770s
# while loops each byte
#                         788.438k (± 1.6%) i/s -      3.998M in   5.071540s
#            each_byte    333.142k (± 1.5%) i/s -      1.688M in   5.068248s
#                regex      1.641M (± 3.1%) i/s -      8.279M in   5.050038s
#                lines    392.103k (± 2.6%) i/s -      1.987M in   5.071725s

# Comparison:
#                regex:  1641119.6 i/s
# while loops each byte:   788437.7 i/s - 2.08x  (± 0.00) slower
# while loops each char:   424273.0 i/s - 3.87x  (± 0.00) slower
#                lines:   392103.3 i/s - 4.19x  (± 0.00) slower
#            each_byte:   333141.9 i/s - 4.93x  (± 0.00) slower
