# frozen_string_literal: true

require_relative '../benchmark_helper'

STRING = 'stringi-pingi'

def slow
  if STRING == 'dupa'
    'dupa'
  elsif STRING == 1
    1
  elsif STRING == 'elo' || STRING == 'stringi-pingi'
    true
  else
    false
  end
end

def fast
  case STRING
  when 'dupa'
    'dupa'
  when 1
    1
  when 'elo', 'stringi-pingi'
    true
  else
    false
  end
end

Benchmark.ips do |x|
  x.report("if")   { slow }
  x.report("case") { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#                   if   555.827k i/100ms
#                 case     1.015M i/100ms
# Calculating -------------------------------------
#                   if      7.162M (± 6.8%) i/s -     36.129M in   5.073726s
#                 case     10.708M (± 4.8%) i/s -     53.804M in   5.037498s

# Comparison:
#                 case: 10708174.0 i/s
#                   if:  7161746.4 i/s - 1.50x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#                   if    36.575k i/100ms
#                 case   504.585k i/100ms
# Calculating -------------------------------------
#                   if     20.505M (± 4.7%) i/s -    101.971M in   4.991151s
#                 case      5.005M (± 1.7%) i/s -     25.229M in   5.042269s

# Comparison:
#                   if: 20505477.5 i/s
#                 case:  5005101.7 i/s - 4.10x  (± 0.00) slower
