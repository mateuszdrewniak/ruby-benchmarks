# frozen_string_literal: true

require_relative '../benchmark_helper'

HASH = Hash[*('a'..'z').to_a]
VAL = 'z'

def hash_lookup
  HASH[VAL].nil?
end

def hash_include
  HASH.include? VAL
end

Benchmark.ips do |x|
  x.report("Hash#[]")   { hash_lookup }
  x.report("Hash#include?") { hash_include }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#              Hash#[]: 13260101.5 i/s
#        Hash#include?: 12229140.8 i/s - 1.08x  (± 0.00) slower
