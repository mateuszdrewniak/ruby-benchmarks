# frozen_string_literal: true

require_relative '../benchmark_helper'

TEXT = <<~TXT
sjkdhfksdhjfwa
asjldhfjlsadf
asljdhfkjasdf
<<XML_BEGIN
kljakldjfklsjdkf
sjldjfklsdf
lksjdf
<<XML_END
sdfijslkdf
slidjflksd
lsjdfsdf
<<XML_BEGIN
kljakldjfklsjdkf
sjldjfklsdf
lksjdf
<<XML_END
sdlkfjslkdf
sldkfjksdf
TXT

def flip_flop
  TEXT.each_line do |line|
    if (line =~ /^<<XML_BEGIN$/)..(line =~ /^<<XML_END$/)
      print #line
    end
  end
end

def normal_code
  in_message = false
  TEXT.each_line do |line|
    in_message = case line
                 when /^<<XML_BEGIN$/
                   true
                 when /^<<XML_END$/
                   false
                 else
                   in_message
                 end

    print if in_message
  end
end

Benchmark.ips do |x|
  x.report("flip flop") { flip_flop }
  x.report("normal code") { normal_code }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#            flip flop:   194788.6 i/s
#          normal code:   145032.5 i/s - 1.34x  (± 0.00) slower
