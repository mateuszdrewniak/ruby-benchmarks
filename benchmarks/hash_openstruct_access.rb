# frozen_string_literal: true

require_relative '../benchmark_helper'

require "ostruct"

HASH = { field_1: 1, field_2: 2}.freeze
OPENSTRUCT = OpenStruct.new(field_1: 1, field_2: 2).freeze

def fast
  [HASH[:field_1], HASH[:field_2]]
end

def slow
  [OPENSTRUCT.field_1, OPENSTRUCT.field_2]
end

Benchmark.ips do |x|
  x.report("Hash")       { fast }
  x.report("OpenStruct") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#                 Hash   881.515k i/100ms
#           OpenStruct   431.358k i/100ms
# Calculating -------------------------------------
#                 Hash      8.305M (±10.6%) i/s -     41.431M in   5.065455s
#           OpenStruct      4.139M (±10.8%) i/s -     20.705M in   5.100310s

# Comparison:
#                 Hash:  8304802.3 i/s
#           OpenStruct:  4138947.9 i/s - 2.01x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#                 Hash    33.271k i/100ms
#           OpenStruct    96.047M i/100ms
# Calculating -------------------------------------
#                 Hash      2.365B (± 8.9%) i/s -     11.021B in   4.844293s
#           OpenStruct    946.856M (± 3.2%) i/s -      4.802B in   5.077863s

# Comparison:
#                 Hash: 2364822511.5 i/s
#           OpenStruct: 946855989.9 i/s - 2.50x  (± 0.00) slower
