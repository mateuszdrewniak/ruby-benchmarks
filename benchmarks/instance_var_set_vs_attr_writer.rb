# frozen_string_literal: true

require_relative '../benchmark_helper'

module TestModule
  @ivar = 3

  class << self
    attr_writer :ivar
  end
end

def ivar_set
  TestModule.instance_variable_set(:@ivar, 3)
end

def attribute_writer
  TestModule.ivar = 3
end

Benchmark.ips do |x|
  x.report('instance_variable_set') { ivar_set }
  x.report('attr_writer') { attribute_writer }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
# instance_variable_set: 12900554.4 i/s
#          attr_writer: 12538094.0 i/s - same-ish: difference falls within error
