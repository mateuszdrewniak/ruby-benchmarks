# frozen_string_literal: true

require_relative '../benchmark_helper'

def eval_string
  ::Module.new.class_eval <<~RUBY
    def dupa(*args)
      puts args
    end
  RUBY
end

def eval_block
  ::Module.new.class_eval do
    def dupa(*args)
      puts args
    end
  end
end

Benchmark.ips do |x|
  x.report('eval string') { eval_string }
  x.report('eval block') { eval_block }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#           eval block:  1683641.7 i/s
#          eval string:    13643.8 i/s - 123.40x  (± 0.00) slower
