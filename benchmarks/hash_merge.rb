# frozen_string_literal: true

require_relative '../benchmark_helper'

ENUM = (1..100)

def slow
  ENUM.inject({}) do |h, e|
    h.merge(e => e)
  end
end

def fast
  ENUM.inject({}) do |h, e|
    h.merge!(e => e)
  end
end

Benchmark.ips do |x|
  x.report('Hash#merge') { slow }
  x.report('Hash#merge!') { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#           Hash#merge   729.000  i/100ms
#          Hash#merge!     4.176k i/100ms
# Calculating -------------------------------------
#           Hash#merge      8.193k (± 4.2%) i/s -     41.553k in   5.081599s
#          Hash#merge!     42.628k (± 3.2%) i/s -    212.976k in   5.001981s

# Comparison:
#          Hash#merge!:    42627.9 i/s
#           Hash#merge:     8193.4 i/s - 5.20x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#           Hash#merge    49.000  i/100ms
#          Hash#merge!    41.960k i/100ms
# Calculating -------------------------------------
#           Hash#merge      7.032k (±13.2%) i/s -     34.300k in   4.989019s
#          Hash#merge!    433.099k (± 3.4%) i/s -      2.182M in   5.045231s

# Comparison:
#          Hash#merge!:   433099.2 i/s
#           Hash#merge:     7032.2 i/s - 61.59x  (± 0.00) slower
