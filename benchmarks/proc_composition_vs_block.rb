# frozen_string_literal: true

require_relative '../benchmark_helper'

TEST_ARRAY = *1000...67546

class Symbol
  def >>(other)
    proc { other.to_proc.(to_proc.(_1)) }
  end

  def <<(other)
    to_proc >> other.to_proc
  end
end

COMPOSED_PROC = :odd?.to_proc >> :to_s.to_proc
def proc_composition_before
  TEST_ARRAY.map &COMPOSED_PROC
end

def proc_composition
  TEST_ARRAY.map &:odd?.to_proc >> :to_s.to_proc
end

def custom_symbol_composition
  TEST_ARRAY.map &:odd? >> :to_s
end

def symbol_composition
  TEST_ARRAY.map &:odd? << :to_s
end

def block
  TEST_ARRAY.map { _1.odd?.to_s }
end

Benchmark.ips do |x|
  x.report('proc composition') { proc_composition }
  x.report('proc composition before') { proc_composition_before }
  x.report('block') { block }
  x.report('custom symbol composition') { custom_symbol_composition }
  x.report('symbol composition') { symbol_composition }
  x.compare!
end

# CRuby 3.1.0
# Comparison:
#                block:      203.5 i/s
# proc composition before:      144.9 i/s - 1.40x  (± 0.00) slower
#     proc composition:      144.5 i/s - 1.41x  (± 0.00) slower
#   symbol composition:      144.5 i/s - 1.41x  (± 0.00) slower
# custom symbol composition:       99.5 i/s - 2.04x  (± 0.00) slower
