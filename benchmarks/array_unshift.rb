# frozen_string_literal: true

require_relative '../benchmark_helper'

def fast
  # Array#unshift
  array = []
  100_000.times { |i| array.unshift(i) }
end

def slow
  # Array#insert
  array = []
  100_000.times { |i| array.insert(0, i) }
end

Benchmark.ips do |x|
  x.report('Array#unshift') { fast }
  x.report('Array#insert') { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#        Array#unshift    13.000  i/100ms
#         Array#insert     1.000  i/100ms
# Calculating -------------------------------------
#        Array#unshift    132.984  (±10.5%) i/s -    663.000  in   5.082508s
#         Array#insert      0.613  (± 0.0%) i/s -      4.000  in   6.521930s

# Comparison:
#        Array#unshift:      133.0 i/s
#         Array#insert:        0.6 i/s - 216.82x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#        Array#unshift     1.000  i/100ms
#         Array#insert     1.000  i/100ms
# Calculating -------------------------------------
#        Array#unshift      0.103  (± 0.0%) i/s -      1.000  in   9.752556s
#         Array#insert      0.922  (± 0.0%) i/s -      5.000  in   5.422305s

# Comparison:
#         Array#insert:        0.9 i/s
#        Array#unshift:        0.1 i/s - 8.99x  (± 0.00) slower
