# frozen_string_literal: true

require_relative '../benchmark_helper'

module TestModule
  @ivar = 3

  class << self
    # attr_accessor :ivar
  end
end

def ivar_get
  true if TestModule.instance_variable_get(:@ivar)
end

def attribute_accessor
  true if TestModule.respond_to? :ivar
end

Benchmark.ips do |x|
  x.report('instance_variable_get') { ivar_get }
  x.report('attr_accessor') { attribute_accessor }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
# instance_variable_set: 12900554.4 i/s
#          attr_writer: 12538094.0 i/s - same-ish: difference falls within error
