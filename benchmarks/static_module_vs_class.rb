# frozen_string_literal: true

require_relative '../benchmark_helper'

class << (TestObject = Object.new)
  def singleton_method
    25 ** 10
  end
end

class TestClass
  def self.class_method
    25 ** 10
  end

  def test_instance_method
    25 ** 10
  end
end

module TestModule
  def self.module_method
    25 ** 10
  end
end

INSTANCE = TestClass.new

TIMES = 1000_000

Benchmark.ips do |x|
  x.report('TestObject::singleton_method') { TIMES.times { TestObject.singleton_method } }
  x.report('TestClass::class_method') { TIMES.times { TestClass.class_method } }
  x.report('TestModule::module_method') { TIMES.times { TestModule.module_method } }
  x.report('TestClass#test_instance_method') { TIMES.times { INSTANCE.test_instance_method } }
  x.compare!
end

# CRuby 3.1.0
# Calculating -------------------------------------
# TestObject::singleton_method
#                          16.901  (± 0.0%) i/s -     85.000  in   5.029681s
# TestClass::class_method
#                          16.753  (± 0.0%) i/s -     84.000  in   5.015024s
# TestModule::module_method
#                          16.823  (± 0.0%) i/s -     85.000  in   5.053154s
# TestClass#test_instance_method
#                          16.789  (± 0.0%) i/s -     84.000  in   5.004258s

# Comparison:
# TestObject::singleton_method:       16.9 i/s
# TestModule::module_method:       16.8 i/s - 1.00x  (± 0.00) slower
# TestClass#test_instance_method:       16.8 i/s - 1.01x  (± 0.00) slower
# TestClass::class_method:       16.8 i/s - 1.01x  (± 0.00) slower
