# frozen_string_literal: true

require_relative '../benchmark_helper'

ARRAY = Array(100)

def fast
  index = 0
  while index < ARRAY.size
    ARRAY[index] + index
    index += 1
  end
  ARRAY
end

def slow
  ARRAY.each_with_index do |number, index|
    number + index
  end
end

Benchmark.ips do |x|
  x.report("While Loop")      { fast }
  x.report("each_with_index") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#           While Loop   986.978k i/100ms
#      each_with_index   414.524k i/100ms
# Calculating -------------------------------------
#           While Loop      9.778M (± 1.0%) i/s -     49.349M in   5.047518s
#      each_with_index      4.087M (± 2.8%) i/s -     20.726M in   5.075999s

# Comparison:
#           While Loop:  9777846.9 i/s
#      each_with_index:  4086790.6 i/s - 2.39x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#           While Loop   176.102k i/100ms
#      each_with_index   244.760M i/100ms
# Calculating -------------------------------------
#           While Loop    164.522M (±12.0%) i/s -    801.616M in   4.965753s
#      each_with_index      2.283B (± 7.2%) i/s -     11.504B in   5.069912s

# Comparison:
#      each_with_index: 2283283057.5 i/s
#           While Loop: 164521967.9 i/s - 13.88x  (± 0.00) slower
