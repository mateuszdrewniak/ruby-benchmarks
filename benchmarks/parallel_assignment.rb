# frozen_string_literal: true

require_relative '../benchmark_helper'

def fast
  _a, _b, _c, _d, _e, _f, _g, _h = 1, 2, 3, 4, 5, 6, 7, 8
  nil
end

def slow
  _a = 1
  _b = 2
  _c = 3
  _d = 4
  _e = 5
  _f = 6
  _g = 7
  _h = 8
  nil
end

Benchmark.ips do |x|
  x.report('Parallel Assignment')   { fast }
  x.report('Sequential Assignment') { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#  Parallel Assignment   844.883k i/100ms
# Sequential Assignment
#                          1.196M i/100ms
# Calculating -------------------------------------
#  Parallel Assignment     11.863M (± 1.0%) i/s -     59.987M in   5.057312s
# Sequential Assignment
#                          11.893M (± 0.7%) i/s -     59.777M in   5.026526s

# Comparison:
# Sequential Assignment: 11892802.2 i/s
#  Parallel Assignment: 11862594.2 i/s - same-ish: difference falls within error


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#  Parallel Assignment    45.761k i/100ms
# Sequential Assignment
#                        261.630M i/100ms
# Calculating -------------------------------------
#  Parallel Assignment      2.311B (±16.1%) i/s -     10.904B in   4.879391s
# Sequential Assignment
#                           2.616B (± 4.3%) i/s -     13.081B in   5.011946s

# Comparison:
# Sequential Assignment: 2615558756.2 i/s
#  Parallel Assignment: 2310963336.3 i/s - same-ish: difference falls within error
