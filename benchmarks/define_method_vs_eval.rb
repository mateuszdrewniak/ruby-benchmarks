# frozen_string_literal: true

require_relative '../benchmark_helper'

OBJECT = ::Object.new

OBJECT.instance_eval <<~RUBY
  def eval_method(*args)
    puts args
  end
RUBY

OBJECT.define_singleton_method(:define_method) do |*args|
  puts args
end

Benchmark.ips do |x|
  x.report('eval method') { OBJECT.eval_method }
  x.report('define method') { OBJECT.define_method }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#          eval method:  5738399.7 i/s
#        define method:  5240870.0 i/s - 1.09x  (± 0.00) slower
