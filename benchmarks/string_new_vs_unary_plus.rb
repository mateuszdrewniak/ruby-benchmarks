# frozen_string_literal: true

require_relative '../benchmark_helper'

Benchmark.ips do |x|
  x.report('String.new') { String.new }
  x.report("+''") { +'' }
  x.compare!
end

# Performance:
# Comparison:
#                  +'': 14685251.8 i/s
#           String.new: 10260788.9 i/s - 1.43x  (± 0.00) slower