# frozen_string_literal: true

require_relative '../benchmark_helper'

def method_with_block
  100.times do
    yield
  end
end

def nested_yield
  method_with_block { yield }
end

def block_to_proc(&block)
  method_with_block(&block)
end

Benchmark.ips do |x|
  x.report('nested yield') { nested_yield { 1 + 1 } }
  x.report('block to Proc') { block_to_proc { 1 + 1 } }
  x.compare!
end

# CRuby 2.6.8
# Comparison:
#        block to Proc:   267765.4 i/s
#         nested yield:   195107.2 i/s - 1.37x  (± 0.00) slower

# CRuby 3.1.1
# Comparison:
#        block to Proc:   238052.6 i/s
#         nested yield:   176143.8 i/s - 1.35x  (± 0.00) slower
