# frozen_string_literal: true

require_relative '../benchmark_helper'

SLUG = 'writing-fast-ruby'

def slow
  SLUG.gsub('-', ' ')
end

def fast
  SLUG.tr('-', ' ')
end

Benchmark.ips do |x|
  x.report('String#gsub') { slow }
  x.report('String#tr')   { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#          String#gsub    53.411k i/100ms
#            String#tr   333.312k i/100ms
# Calculating -------------------------------------
#          String#gsub    603.000k (± 1.8%) i/s -      3.044M in   5.050554s
#            String#tr      3.419M (± 1.9%) i/s -     17.332M in   5.070667s

# Comparison:
#            String#tr:  3419468.4 i/s
#          String#gsub:   602999.6 i/s - 5.67x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#          String#gsub     3.395k i/100ms
#            String#tr   120.633k i/100ms
# Calculating -------------------------------------
#          String#gsub      2.059M (±14.5%) i/s -      9.825M in   4.966840s
#            String#tr      1.630M (±26.9%) i/s -      6.997M in   5.043341s

# Comparison:
#          String#gsub:  2059436.1 i/s
#            String#tr:  1629509.6 i/s - same-ish: difference falls within error
