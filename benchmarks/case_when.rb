# frozen_string_literal: true

require_relative '../benchmark_helper'

class Example
  class << self
    alias [] new
  end

  attr_reader :name

  def initialize(name)
    @name = name
  end

  # metoda porównywania w `case when`
  def ===(other)
    puts "Porównuję #{self.name.inspect} z #{other.inspect}"
  end
end

case 'moj string'
when Example[:jakas], Example['wartosc'], Example['inna']
  3
when Example[1], Example[2], Example[3]
  2
else
  1
end

# Porównuję :jakas z "moj string"
# Porównuję "wartosc" z "moj string"
# Porównuję "inna" z "moj string"
# Porównuję 1 z "moj string"
# Porównuję 2 z "moj string"
# Porównuję 3 z "moj string"
