# frozen_string_literal: true

require_relative '../benchmark_helper'

HASH = Hash[*("a".."zzz").to_a.shuffle]
VALUE = "zz"

def value_fast
  HASH.value? VALUE
end

def value_slow
  HASH.values.include? VALUE
end

Benchmark.ips do |x|
  x.report("Hash#values.include?") { value_slow }
  x.report("Hash#value?") { value_fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
# Hash#values.include?   864.000  i/100ms
#          Hash#value?     1.391k i/100ms
# Calculating -------------------------------------
# Hash#values.include?      9.560k (± 3.1%) i/s -     48.384k in   5.066525s
#          Hash#value?     13.852k (± 1.5%) i/s -     69.550k in   5.022098s

# Comparison:
#          Hash#value?:    13852.2 i/s
# Hash#values.include?:     9559.5 i/s - 1.45x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
# Hash#values.include?     4.000  i/100ms
#          Hash#value?    85.000  i/100ms
# Calculating -------------------------------------
# Hash#values.include?    472.698  (±15.7%) i/s -      2.252k in   4.987226s
#          Hash#value?      1.192k (±15.5%) i/s -      5.865k in   5.066287s

# Comparison:
#          Hash#value?:     1192.1 i/s
# Hash#values.include?:      472.7 i/s - 2.52x  (± 0.00) slower
