# frozen_string_literal: true

require_relative '../benchmark_helper'

class User
  attr_accessor :first_name

  def last_name
    @last_name
  end

  def last_name=(value)
    @last_name = value
  end

end

def slow
  user = User.new
  user.last_name = 'John'
  user.last_name
end

def fast
  user = User.new
  user.first_name = 'John'
  user.first_name
end

Benchmark.ips do |x|
  x.report('getter_and_setter') { slow }
  x.report('attr_accessor')     { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#    getter_and_setter   409.138k i/100ms
#        attr_accessor   447.598k i/100ms
# Calculating -------------------------------------
#    getter_and_setter      4.134M (± 2.1%) i/s -     20.866M in   5.050050s
#        attr_accessor      4.458M (± 1.4%) i/s -     22.380M in   5.020785s

# Comparison:
#        attr_accessor:  4458332.0 i/s
#    getter_and_setter:  4133772.0 i/s - 1.08x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#    getter_and_setter    19.402k i/100ms
#        attr_accessor   264.849M i/100ms
# Calculating -------------------------------------
#    getter_and_setter      2.428B (±11.5%) i/s -     11.510B in   4.842229s
#        attr_accessor      2.658B (± 1.9%) i/s -     13.507B in   5.083902s

# Comparison:
#        attr_accessor: 2657874177.5 i/s
#    getter_and_setter: 2427742059.6 i/s - same-ish: difference falls within error
