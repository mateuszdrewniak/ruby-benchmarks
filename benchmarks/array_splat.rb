# frozen_string_literal: true

require_relative '../benchmark_helper'

module M
  ITEMS = (1..10000).to_a.freeze

  def self.func(*args)
  end
end

def fast
  M.func(M::ITEMS)
end

def slow
  M.func(*M::ITEMS)
end

Benchmark.ips do |x|
  x.report("array param") { fast }
  x.report("splat param") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#          array param   766.567k i/100ms
#          splat param     2.044k i/100ms
# Calculating -------------------------------------
#          array param      7.537M (± 6.2%) i/s -     37.562M in   5.009845s
#          splat param     20.130k (± 4.4%) i/s -    102.200k in   5.087659s

# Comparison:
#          array param:  7536836.4 i/s
#          splat param:    20130.1 i/s - 374.41x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#          array param    64.172k i/100ms
#          splat param     1.006k i/100ms
# Calculating -------------------------------------
#          array param      2.385B (±13.0%) i/s -     11.216B in   4.851170s
#          splat param      9.937k (± 4.6%) i/s -     50.300k in   5.074731s

# Comparison:
#          array param: 2384958064.8 i/s
#          splat param:     9937.2 i/s - 240002.27x  (± 0.00) slower
