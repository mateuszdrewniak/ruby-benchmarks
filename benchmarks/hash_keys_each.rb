# frozen_string_literal: true

require_relative '../benchmark_helper'

HASH = {
  'provider' => 'facebook',
  'uid' => '1234567',
  'info' => {
    'nickname' => 'jbloggs',
    'email' => 'joe@bloggs.com',
    'name' => 'Joe Bloggs',
    'first_name' => 'Joe',
    'last_name' => 'Bloggs',
    'image' => 'http://graph.facebook.com/1234567/picture?type=square',
    'urls' => { 'Facebook' => 'http://www.facebook.com/jbloggs' },
    'location' => 'Palo Alto, California',
    'verified' => true
  },
  'credentials' => {
    'token' => 'ABCDEF...',
    'expires_at' => 1321747205,
    'expires' => true
  },
  'extra' => {
    'raw_info' => {
      'id' => '1234567',
      'name' => 'Joe Bloggs',
      'first_name' => 'Joe',
      'last_name' => 'Bloggs',
      'link' => 'http://www.facebook.com/jbloggs',
      'username' => 'jbloggs',
      'location' => { 'id' => '123456789', 'name' => 'Palo Alto, California' },
      'gender' => 'male',
      'email' => 'joe@bloggs.com',
      'timezone' => -8,
      'locale' => 'en_US',
      'verified' => true,
      'updated_time' => '2011-11-11T06:21:03+0000'
    }
  }
}

def slow
  HASH.keys.each(&:to_sym)
end

def fast
  HASH.each_key(&:to_sym)
end

Benchmark.ips do |x|
  x.report('Hash#keys.each') { slow }
  x.report('Hash#each_key')  { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#       Hash#keys.each   100.150k i/100ms
#        Hash#each_key   163.632k i/100ms
# Calculating -------------------------------------
#       Hash#keys.each      1.335M (±16.7%) i/s -      6.410M in   5.030238s
#        Hash#each_key      1.624M (± 1.8%) i/s -      8.182M in   5.041170s

# Comparison:
#        Hash#each_key:  1623518.1 i/s
#       Hash#keys.each:  1334779.5 i/s - 1.22x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#       Hash#keys.each     4.316k i/100ms
#        Hash#each_key   207.747k i/100ms
# Calculating -------------------------------------
#       Hash#keys.each      1.796M (±11.1%) i/s -      8.736M in   4.968817s
#        Hash#each_key      2.060M (± 3.1%) i/s -     10.387M in   5.048769s

# Comparison:
#        Hash#each_key:  2059683.1 i/s
#       Hash#keys.each:  1795681.3 i/s - 1.15x  (± 0.00) slower
