# frozen_string_literal: true

require_relative '../benchmark_helper'

NUMBER = 100_000_000

def fast
  index = 0
  while true
    break if index > NUMBER
    index += 1
  end
end

def slow
  index = 0
  loop do
    break if index > NUMBER
    index += 1
  end
end

Benchmark.ips do |x|
  x.report("While Loop")  { fast }
  x.report("Kernel loop") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#           While Loop     1.000  i/100ms
#          Kernel loop     1.000  i/100ms
# Calculating -------------------------------------
#           While Loop      0.639  (± 0.0%) i/s -      4.000  in   6.264543s
#          Kernel loop      0.199  (± 0.0%) i/s -      1.000  in   5.017473s

# Comparison:
#           While Loop:        0.6 i/s
#          Kernel loop:        0.2 i/s - 3.21x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#           While Loop     1.000  i/100ms
#          Kernel loop     1.000  i/100ms
# Calculating -------------------------------------
#           While Loop     26.353  (± 3.8%) i/s -    132.000  in   5.016696s
#          Kernel loop      4.384  (± 0.0%) i/s -     22.000  in   5.031445s

# Comparison:
#           While Loop:       26.4 i/s
#          Kernel loop:        4.4 i/s - 6.01x  (± 0.00) slower
