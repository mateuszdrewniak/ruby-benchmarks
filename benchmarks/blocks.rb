# frozen_string_literal: true

require_relative '../benchmark_helper'

def slow
  [1, 2, 3].map { |n| n.to_f }
end

def fast
  [1, 2, 3].map(&:to_f)
end

Benchmark.ips do |x|
  x.report("normal")  { slow }
  x.report("&method") { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#               normal   298.812k i/100ms
#              &method   323.950k i/100ms
# Calculating -------------------------------------
#               normal      2.970M (± 1.3%) i/s -     14.941M in   5.032270s
#              &method      3.223M (± 1.6%) i/s -     16.198M in   5.026671s

# Comparison:
#              &method:  3223173.1 i/s
#               normal:  2969508.5 i/s - 1.09x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#               normal    19.939k i/100ms
#              &method   262.116M i/100ms
# Calculating -------------------------------------
#               normal      2.393B (±13.3%) i/s -     11.104B in   4.828736s
#              &method      2.585B (± 4.3%) i/s -     13.106B in   5.081608s

# Comparison:
#              &method: 2584717876.8 i/s
#               normal: 2392668706.8 i/s - same-ish: difference falls within error
