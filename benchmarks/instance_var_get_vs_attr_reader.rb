# frozen_string_literal: true

require_relative '../benchmark_helper'

module TestModule
  @ivar = 3

  class << self
    attr_reader :ivar
  end
end

def ivar_get
  TestModule.instance_variable_get(:@ivar)
end

def attribute_reader
  TestModule.ivar
end

Benchmark.ips do |x|
  x.report('instance_variable_get') { ivar_get }
  x.report('attr_reader') { attribute_reader }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#          attr_reader: 14181750.6 i/s
# instance_variable_get: 13177347.0 i/s - 1.08x  (± 0.00) slower
