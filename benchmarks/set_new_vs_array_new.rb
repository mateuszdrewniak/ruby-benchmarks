# frozen_string_literal: true

require_relative '../benchmark_helper'

def set_new
  ::Set[1, 2]
end

def array_new
  [1, 2]
end

def hash_new
  h = Hash.new
  h[1] = true
  h[2] = true
end

Benchmark.ips do |x|
  x.report('Set::new') { set_new }
  x.report('Array::new') { array_new }
  x.report('Hash::new') { hash_new }
  x.compare!
end

# CRuby 3.1.2

