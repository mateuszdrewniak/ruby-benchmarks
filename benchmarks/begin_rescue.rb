# frozen_string_literal: true

require_relative '../benchmark_helper'

def slow
  begin
    writing
  rescue
    'fast ruby'
  end
end

def fast
  if respond_to?(:writing)
    writing
  else
    'fast ruby'
  end
end

Benchmark.ips do |x|
  x.report('begin...rescue') { slow }
  x.report('respond_to?')    { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#       begin...rescue    66.060k i/100ms
#          respond_to?   806.981k i/100ms
# Calculating -------------------------------------
#       begin...rescue    660.363k (± 5.5%) i/s -      3.303M in   5.019033s
#          respond_to?      7.630M (± 8.5%) i/s -     37.928M in   5.016883s

# Comparison:
#          respond_to?:  7629510.3 i/s
#       begin...rescue:   660362.9 i/s - 11.55x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#       begin...rescue     4.827k i/100ms
#          respond_to?   256.268M i/100ms
# Calculating -------------------------------------
#       begin...rescue     73.081k (± 6.8%) i/s -    366.852k in   5.045921s
#          respond_to?      2.545B (± 3.8%) i/s -     12.813B in   5.042453s

# Comparison:
#          respond_to?: 2544981188.5 i/s
#       begin...rescue:    73080.6 i/s - 34824.30x  (± 0.00) slower
