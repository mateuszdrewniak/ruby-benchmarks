# frozen_string_literal: true

require_relative '../benchmark_helper'

require "ostruct"

HASH = { field1: 1, field2: 2}
OPENSTRUCT = OpenStruct.new(field1: 1, field2: 2)

def fast
  HASH[:dupa] = 3
  HASH[:origami] = 'Origami moje'
end

def slow
  OPENSTRUCT[:dupa] = 3
  OPENSTRUCT[:origami] = 'Origami moje'
end

Benchmark.ips do |x|
  x.report("Hash")       { fast }
  x.report("OpenStruct") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#                 Hash   731.676k i/100ms
#           OpenStruct   182.945k i/100ms
# Calculating -------------------------------------
#                 Hash      8.532M (± 4.1%) i/s -     43.169M in   5.069994s
#           OpenStruct      2.103M (± 1.5%) i/s -     10.611M in   5.045833s

# Comparison:
#                 Hash:  8531527.8 i/s
#           OpenStruct:  2103411.9 i/s - 4.06x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#                 Hash    30.123k i/100ms
#           OpenStruct   555.496k i/100ms
# Calculating -------------------------------------
#                 Hash     47.594M (± 4.1%) i/s -    236.676M in   4.986713s
#           OpenStruct      5.508M (± 2.5%) i/s -     27.775M in   5.046285s

# Comparison:
#                 Hash: 47593811.1 i/s
#           OpenStruct:  5507800.6 i/s - 8.64x  (± 0.00) slower
