# frozen_string_literal: true

require_relative '../benchmark_helper'

module Some
  CONSTANT_2 = 5

  module Test
    CONSTANT_1 = 3

    module Module
      module Structure

        def self.relative_path
          CONSTANT_1 + CONSTANT_2
        end

        def self.incomplete_absolute_path
          Some::Test::CONSTANT_1 + Some::CONSTANT_2
        end

        def self.absolute_path
          ::Some::Test::CONSTANT_1 + ::Some::CONSTANT_2
        end
      end
    end
  end
end

TIMES = 1000_000

Benchmark.ips do |x|
  x.report('relative constant path') { TIMES.times { ::Some::Test::Module::Structure.relative_path } }
  x.report('absolute constant path') { TIMES.times { ::Some::Test::Module::Structure.absolute_path } }
  x.report('incomplete absolute constant path') { TIMES.times {::Some::Test::Module::Structure.incomplete_absolute_path } }
  x.compare!
end

# CRuby 3.1.0
# Comparison:
# relative constant path:       19.1 i/s
# absolute constant path:       19.0 i/s - 1.00x  (± 0.00) slower
# incomplete absolute constant path:       18.6 i/s - 1.02x  (± 0.00) slower
