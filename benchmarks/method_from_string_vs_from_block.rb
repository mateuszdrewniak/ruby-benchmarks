# frozen_string_literal: true

require_relative '../benchmark_helper'

OBJECT = ::Object.new

OBJECT.instance_eval <<~RUBY
  def method_from_string(*args)
    puts args
  end
RUBY

OBJECT.instance_eval do
  def method_from_block(*args)
    puts args
  end
end

Benchmark.ips do |x|
  x.report('method from string') { OBJECT.method_from_string }
  x.report('method from block') { OBJECT.method_from_block }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#    method from block:  5801342.7 i/s
#   method from string:  5792291.7 i/s - same-ish: difference falls within error
