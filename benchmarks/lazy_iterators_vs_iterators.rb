# frozen_string_literal: true

require_relative '../benchmark_helper'

NUMS = (0...1_000_000)


def lazy_iterator
  NUMS.lazy
      .select(&:odd?)
      .map(&:to_s)
      .first(100)
end

def iterator
  NUMS.first(100)
      .select(&:odd?)
      .map(&:to_s)
end

Benchmark.ips do |x|
  x.report('lazy iterator') { lazy_iterator }
  x.report('iterator') { iterator }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#           eval block:  1683641.7 i/s
#          eval string:    13643.8 i/s - 123.40x  (± 0.00) slower
