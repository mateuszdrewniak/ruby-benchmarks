# frozen_string_literal: true

require_relative '../benchmark_helper'
require 'base64'

CONFIG = {
  something: {
    enabled: false
  }.freeze
}.freeze

def condition_in_method
  if CONFIG.dig(:something, :enabled)
    1 + 2
  end

  5 * 8
end

def conditional_method_definition
  5 * 8
end

if CONFIG.dig(:something, :enabled)
  alias orig_conditional_method_definition conditional_method_definition

  def conditional_method_definition
    1 + 2
    orig_conditional_method_definition
  end
end

Benchmark.ips do |x|
  x.report('condition_in_method') do
    condition_in_method
  end
  x.report('conditional_method_definition') do
    conditional_method_definition
  end
  x.compare!
end

# CRuby 3.2.0
# Comparison:
# conditional_method_definition: 19900916.6 i/s
#  condition_in_method: 11692511.1 i/s - 1.70x  (± 0.00) slower
