# frozen_string_literal: true

require_relative '../benchmark_helper'

NUMBER = 40

module RecursiveFibonacci
  class << self
    def at(index)
      return index if (0..1).include?(index)

      at(index - 1) + at(index - 2)
    end
  end
end

class DynamicFibonacci
  include ::Enumerable

  class << self
    def at(index)
      new.at(index)
    end
  end

  attr_reader :cache

  def initialize
    reset_cache
  end

  # @param index [Integer]
  # @return [Integer]
  def at(index)
    return @cache[index] if @cache[index]

    while index >= @cache.length
      @cache[@cache.length] = @cache[@cache.length - 1] + @cache[@cache.length - 2]
    end

    @cache[index]
  end

  # @return [Enumerator, self]
  # @yieldparam [Integer]
  def each
    return enum_for(:each) unless block_given?

    (0..).each do |i|
      yield at(i)
    end

    self
  end

  # @return [void]
  def reset_cache
    @cache = [0, 1, 1]
  end
end



def recursive
  RecursiveFibonacci.at(NUMBER)
end

def dynamic
  DynamicFibonacci.at(NUMBER)
end

Benchmark.ips do |x|
  x.report('recursive') { recursive }
  x.report('dynamic') { dynamic }
  x.compare!
end

# CRuby 3.2.0
# Comparison:
#              dynamic:   383671.3 i/s
#            recursive:        0.0 i/s - 9660751.41x  (± 0.00) slower
