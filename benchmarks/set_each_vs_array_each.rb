# frozen_string_literal: true

require_relative '../benchmark_helper'

ARRAY = (1..100).to_a
SET = ARRAY.to_set

def set_each
  SET.each { |e| e + 1 }
end

def array_each
  ARRAY.each { |e| e + 1 }
end

Benchmark.ips do |x|
  x.report('Set#each') { set_each }
  x.report('Array#each') { array_each }
  x.compare!
end

# CRuby 3.1.2

