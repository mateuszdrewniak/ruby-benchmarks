# frozen_string_literal: true

require_relative '../benchmark_helper'

SLUG = 'writing-fast-ruby-writing'

def slow
  SLUG.gsub(/[writ]/, { 'w' => 'd', 'r' => 'u', 'i' => 'p', 't' => 'a' })
end

def fast
  SLUG.tr('writ', 'dupa')
end

Benchmark.ips do |x|
  x.report('String#gsub') { slow }
  x.report('String#tr')   { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#          String#gsub     8.757k i/100ms
#            String#tr    66.241k i/100ms
# Calculating -------------------------------------
#          String#gsub     88.920k (±31.3%) i/s -    385.308k in   5.048348s
#            String#tr    650.390k (±29.3%) i/s -      3.047M in   5.113797s

# Comparison:
#            String#tr:   650389.6 i/s
#          String#gsub:    88920.2 i/s - 7.31x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#          String#gsub   806.000  i/100ms
#            String#tr    83.698k i/100ms
# Calculating -------------------------------------
#          String#gsub    294.804k (± 9.3%) i/s -      1.448M in   4.969262s
#            String#tr      1.357M (± 4.8%) i/s -      6.780M in   5.009315s

# Comparison:
#            String#tr:  1356811.3 i/s
#          String#gsub:   294804.3 i/s - 4.60x  (± 0.00) slower
