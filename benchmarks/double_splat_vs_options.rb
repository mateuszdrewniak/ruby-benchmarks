# frozen_string_literal: true

require_relative '../benchmark_helper'
require 'date'

def options_hash(options = {})
end

def double_splat(**kwargs)
end

Benchmark.ips do |x|
  x.report('double splat') { double_splat dupa: 3, elo: { siema: 5 } }
  x.report('options hash') { options_hash dupa: 3, elo: { siema: 5 } }
  x.compare!
end

# CRuby 3.1.2
# Comparison:
#         options hash:  7895274.9 i/s
#         double splat:  7726640.5 i/s - same-ish: difference falls within error
