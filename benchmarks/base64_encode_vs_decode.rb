# frozen_string_literal: true

require_relative '../benchmark_helper'
require 'base64'

PAYLOAD = "ljashdfilahwdlkjfhasjkdblvkljasdf"
ENCODED = Base64.encode64 PAYLOAD


def encode
  Base64.encode64 PAYLOAD
end

def decode
  Base64.decode64 ENCODED
end

Benchmark.ips do |x|
  x.report('encode') { encode }
  x.report('decode') { decode }
  x.compare!
end

# CRuby 3.2.0
# Comparison:
#               decode:  6338053.2 i/s
#               encode:  3479537.2 i/s - 1.82x  (± 0.00) slower

# CRuby YJIT 3.2.0
# Comparison:
#               decode:  9912924.0 i/s
#               encode:  4600622.0 i/s - 2.15x  (± 0.00) slower
