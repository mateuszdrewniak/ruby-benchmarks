# frozen_string_literal: true

require_relative '../benchmark_helper'

# 2 + 1 = 3 object
def slow_plus
  'foo' + 'bar'
end

# 2 + 1 = 3 object
def slow_concat
  'foo'.concat 'bar'
end

# 2 + 1 = 3 object
def slow_append
  'foo' << 'bar'
end

def fast_interpolation
  "#{'foo'}#{'bar'}"
end

Benchmark.ips do |x|
  x.report('String#+')                 { slow_plus }
  x.report('String#concat')            { slow_concat }
  x.report('String#append')            { slow_append }
  x.report('"#{\'foo\'}#{\'bar\'}"')   { fast_interpolation }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#             String#+   594.540k i/100ms
#        String#concat   551.567k i/100ms
#        String#append   603.827k i/100ms
#   "#{'foo'}#{'bar'}"     1.238M i/100ms
# Calculating -------------------------------------
#             String#+      5.916M (± 1.9%) i/s -     29.727M in   5.027024s
#        String#concat      5.462M (± 4.0%) i/s -     27.578M in   5.059808s
#        String#append      6.075M (± 1.6%) i/s -     30.795M in   5.070415s
#   "#{'foo'}#{'bar'}"     12.316M (± 1.9%) i/s -     61.901M in   5.027850s

# Comparison:
#   "#{'foo'}#{'bar'}": 12316361.0 i/s
#        String#append:  6075200.7 i/s - 2.03x  (± 0.00) slower
#             String#+:  5915877.6 i/s - 2.08x  (± 0.00) slower
#        String#concat:  5461639.8 i/s - 2.26x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#             String#+    45.524k i/100ms
#        String#concat   261.186M i/100ms
#        String#append   256.201M i/100ms
#   "#{'foo'}#{'bar'}"   258.865M i/100ms
# Calculating -------------------------------------
#             String#+      2.262B (±17.8%) i/s -     10.555B in   4.868863s
#        String#concat      2.593B (± 2.9%) i/s -     13.059B in   5.040660s
#        String#append      2.560B (± 6.4%) i/s -     12.810B in   5.032708s
#   "#{'foo'}#{'bar'}"      2.546B (± 4.1%) i/s -     12.943B in   5.092163s

# Comparison:
#        String#concat: 2593085199.0 i/s
#        String#append: 2560106972.2 i/s - same-ish: difference falls within error
#   "#{'foo'}#{'bar'}": 2546219416.1 i/s - same-ish: difference falls within error
#             String#+: 2262034929.2 i/s - same-ish: difference falls within error