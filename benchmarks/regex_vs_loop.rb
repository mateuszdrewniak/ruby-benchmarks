# frozen_string_literal: true

require_relative '../benchmark_helper'

PAYLOAD = <<~STR
-----BEGIN PGP MESSAGE-----
ZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGFkdXBhIHRydXBhIHNpxJkgcm96a8WCYWRhIGxhIGxhIGxhIGxhZHVwYSB0cnVwYSBzacSZIHJvemvFgmFkYSBsYSBsYSBsYSBsYWR1cGEgdHJ1cGEgc2nEmSByb3prxYJhZGEgbGEgbGEgbGEgbGE=
-----END PGP MESSAGE-----
STR

def while_loops_each_char
  str = PAYLOAD
  first_newline_index = 0

  i = 0
  strlen = str.length
  while i < strlen
    break first_newline_index = i if str[i] == "\n"

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str[i] == "\n"

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

NEWLINE_BYTE = "\n".bytes.first

def while_loops_each_byte
  str = PAYLOAD
  newline_byte = NEWLINE_BYTE
  first_newline_index = 0

  i = 0
  strlen = str.length
  while i < strlen
    break first_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

def lines
  PAYLOAD.lines[1..-2]
end

def each_byte
  str = PAYLOAD
  newline_byte = NEWLINE_BYTE
  first_newline_index = 0

  i = 0
  strlen = str.length

  str.each_byte.with_index do |byte, i|
    break first_newline_index = i if byte == newline_byte

    i += 1
  end

  last_newline_index = strlen - 1

  while i >= 0
    break last_newline_index = i if str.getbyte(i) == newline_byte

    i += 1
  end

  str[first_newline_index+1, last_newline_index-2]
end

def regex
  PAYLOAD[/-----BEGIN PGP MESSAGE-----\n(.*)\n-----END PGP MESSAGE-----/, 1]
end

Benchmark.ips do |x|
  x.report('while loops each char') { while_loops_each_char }
  x.report('while loops each byte') { while_loops_each_byte }
  x.report('each_byte') { each_byte }
  x.report('regex') { regex }
  x.report('lines') { lines }
  x.compare!
end

# Warming up --------------------------------------
# while loops each char
#                         44.288k i/100ms
# while loops each byte
#                         77.319k i/100ms
#            each_byte    34.147k i/100ms
#                regex    36.194k i/100ms
#                lines   208.912k i/100ms
# Calculating -------------------------------------
# while loops each char
#                         429.893k (± 1.6%) i/s -      2.170M in   5.049353s
# while loops each byte
#                         787.344k (± 2.3%) i/s -      3.943M in   5.011113s
#            each_byte    331.523k (± 2.2%) i/s -      1.673M in   5.049589s
#                regex    371.823k (± 4.2%) i/s -      1.882M in   5.071402s
#                lines      2.115M (± 2.6%) i/s -     10.655M in   5.040331s

# Comparison:
#                lines:  2115310.9 i/s
# while loops each byte:   787343.8 i/s - 2.69x  (± 0.00) slower
# while loops each char:   429893.1 i/s - 4.92x  (± 0.00) slower
#                regex:   371822.9 i/s - 5.69x  (± 0.00) slower
#            each_byte:   331523.3 i/s - 6.38x  (± 0.00) slower
