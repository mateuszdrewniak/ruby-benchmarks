# frozen_string_literal: true

require_relative '../benchmark_helper'

require 'securerandom'

m = ::Module.new
m.define_singleton_method(:encode) do |val|
  val = val.to_s
  val = val.length.even? ? val : "0#{val}"
  raise ArgumentError, "Invalid value: #{val.inspect} must be numeric!" unless val =~ /^[0-9]*$/

  [val].pack("H*")
end
CodecModule = m

class CodecClass
  attr_accessor :encoder

  def encode(val)
    encoder.call(val)
  end
end

CODEC_INSTANCE = CodecClass.new
CODEC_INSTANCE.encoder = proc do |val|
  val = val.to_s
  val = val.length.even? ? val : "0#{val}"
  raise Error, "Invalid value: #{val.inspect} must be numeric!" unless val =~ /^[0-9]*$/

  [val].pack("H*")
end

TEST_CASES = Array.new(100_000) { SecureRandom.random_number(1000_000) }.freeze

Benchmark.ips do |x|
  x.report('Module singleton methods') { TEST_CASES.each { |val| CodecModule.encode(val) } }
  x.report('Class instance with Procs') { TEST_CASES.each { |val| CODEC_INSTANCE.encode(val) } }
  x.compare!
end

# CRuby 3.1.0
# Comparison:
# Module singleton methods:       16.1 i/s
# Class instance with Procs:       15.7 i/s - 1.03x  (± 0.00) slower
