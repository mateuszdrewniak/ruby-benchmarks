# frozen_string_literal: true

require_relative '../benchmark_helper'

ARRAY = [*1..100]

def fastest
  # Array#length
  ARRAY.length
end

def fast
  # Array#size
  ARRAY.size
end

def slow
  # Array#count
  ARRAY.count
end

Benchmark.ips do |x|
  x.report("Array#length") { fastest }
  x.report("Array#size") { fast }
  x.report("Array#count") { slowest }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#         Array#length     1.510M i/100ms
#           Array#size     1.590M i/100ms
#          Array#count     1.142M i/100ms
# Calculating -------------------------------------
#         Array#length     15.045M (± 3.0%) i/s -     75.506M in   5.023722s
#           Array#size     15.636M (± 2.8%) i/s -     79.510M in   5.089757s
#          Array#count     11.307M (± 2.4%) i/s -     57.091M in   5.052334s

# Comparison:
#           Array#size: 15635595.8 i/s
#         Array#length: 15044674.8 i/s - same-ish: difference falls within error
#          Array#count: 11306948.1 i/s - 1.38x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#         Array#length    67.395k i/100ms
#           Array#size   264.061M i/100ms
#          Array#count   258.102M i/100ms
# Calculating -------------------------------------
#         Array#length      2.092B (±18.9%) i/s -      9.756B in   4.869026s
#           Array#size      2.374B (±11.7%) i/s -     11.883B in   5.093759s
#          Array#count      2.388B (± 9.1%) i/s -     11.873B in   5.035463s

# Comparison:
#          Array#count: 2387804745.7 i/s
#           Array#size: 2373771764.8 i/s - same-ish: difference falls within error
#         Array#length: 2092409927.9 i/s - same-ish: difference falls within error
