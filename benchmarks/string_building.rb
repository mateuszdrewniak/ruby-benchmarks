# frozen_string_literal: true

require_relative '../benchmark_helper'

STR1 = "dupa"
STR2 = "elo"
STR3 = "siema"

def plus_equals
  title =  'Podsumowanie żądań zwrotnych'
  title += " dla serwisu #{STR1}" if STR1
  title += " od #{STR2}" if STR2
  title += " do #{STR3}" if STR3
  title
end

def plus_equals_shovel
  title =  'Podsumowanie żądań zwrotnych'
  (title += " dla serwisu ") << STR1 if STR1
  (title += " od ") << STR2 if STR2
  (title += " do ") << STR3 if STR3
  title
end

def interpolation
  title = 'Podsumowanie żądań zwrotnych'
  title = "#{title} dla serwisu #{STR1}" if STR1
  title = "#{title} #{STR2}" if STR2
  title = "#{title} #{STR3}"   if STR3
  title
end

def shovel_interpolation
  title = ::String.new
  title << 'Podsumowanie żądań zwrotnych'
  title << " dla serwisu #{STR1}" if STR1
  title << " od #{STR2}" if STR2
  title << " do #{STR3}" if STR3
  title
end

def shovel
  title = ::String.new
  title << 'Podsumowanie żądań zwrotnych'
  title << " dla serwisu " << STR1 if STR1
  title << " od " << STR2 if STR2
  title << " do " << STR3 if STR3
  title
end

Benchmark.ips do |x|
  x.report('plus equals') { plus_equals }
  x.report('plus equals shovel') { plus_equals_shovel }
  x.report('interpolation') { interpolation }
  x.report('shovel interpolation') { shovel_interpolation }
  x.report('shovel') { shovel }
  x.compare!
end

# Benchmark.memory do |x|
#   x.report('plus equals') { plus_equals }
#   x.report('plus equals shovel') { plus_equals_shovel }
#   x.report('interpolation') { interpolation }
#   x.report('shovel interpolation') { shovel_interpolation }
#   x.report('shovel') { shovel }
#   x.compare!
# end

# Performance:
# Comparison:
#   plus equals shovel:  3100464.6 i/s
#        interpolation:  2317786.6 i/s - 1.34x  (± 0.00) slower
#          plus equals:  2062744.5 i/s - 1.50x  (± 0.00) slower
#               shovel:  1949868.7 i/s - 1.59x  (± 0.00) slower
# shovel interpolation:  1361621.4 i/s - 2.28x  (± 0.00) slower

# Memory
# Comparison:
#   plus equals shovel:  3100464.6 i/s
#        interpolation:  2317786.6 i/s - 1.34x  (± 0.00) slower
#          plus equals:  2062744.5 i/s - 1.50x  (± 0.00) slower
#               shovel:  1949868.7 i/s - 1.59x  (± 0.00) slower
# shovel interpolation:  1361621.4 i/s - 2.28x  (± 0.00) slower
