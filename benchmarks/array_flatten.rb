# frozen_string_literal: true

require_relative '../benchmark_helper'

ARRAY = (1..100).to_a

def slowest
  ARRAY.map { |e| [e, e] }.flatten
end

def slow
  ARRAY.map { |e| [e, e] }.flatten(1)
end

def fast
  ARRAY.flat_map { |e| [e, e] }
end

Benchmark.ips do |x|
  x.report('Array#map.flatten(1)') { slow }
  x.report('Array#map.flatten')    { slowest }
  x.report('Array#flat_map')       { fast }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
# Array#map.flatten(1)     4.984k i/100ms
#    Array#map.flatten     3.252k i/100ms
#       Array#flat_map     8.490k i/100ms
# Calculating -------------------------------------
# Array#map.flatten(1)     55.327k (± 2.2%) i/s -    279.104k in   5.047248s
#    Array#map.flatten     36.614k (± 1.6%) i/s -    185.364k in   5.064229s
#       Array#flat_map     84.765k (± 2.3%) i/s -    424.500k in   5.010707s

# Comparison:
#       Array#flat_map:    84765.5 i/s
# Array#map.flatten(1):    55327.0 i/s - 1.53x  (± 0.00) slower
#    Array#map.flatten:    36613.5 i/s - 2.32x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
# Array#map.flatten(1)   770.000  i/100ms
#    Array#map.flatten    18.598k i/100ms
#       Array#flat_map    45.242k i/100ms
# Calculating -------------------------------------
# Array#map.flatten(1)    186.340k (± 8.0%) i/s -    920.150k in   4.975387s
#    Array#map.flatten    182.978k (± 4.3%) i/s -    929.900k in   5.093526s
#       Array#flat_map    450.362k (± 2.0%) i/s -      2.262M in   5.025071s

# Comparison:
#       Array#flat_map:   450361.5 i/s
# Array#map.flatten(1):   186339.6 i/s - 2.42x  (± 0.00) slower
#    Array#map.flatten:   182977.8 i/s - 2.46x  (± 0.00) slower
