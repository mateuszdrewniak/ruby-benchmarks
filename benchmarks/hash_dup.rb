# frozen_string_literal: true

require_relative '../benchmark_helper'

HASH = Hash[*('a'..'z').to_a]

def fast
  Hash[HASH]
end

def slow
  HASH.dup
end

Benchmark.ips do |x|
  x.report("Hash[]")   { fast }
  x.report("Hash#dup") { slow }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#               Hash[]   126.745k i/100ms
#             Hash#dup    57.240k i/100ms
# Calculating -------------------------------------
#               Hash[]      1.341M (± 4.0%) i/s -      6.717M in   5.017780s
#             Hash#dup    566.123k (± 2.7%) i/s -      2.862M in   5.059543s

# Comparison:
#               Hash[]:  1340920.9 i/s
#             Hash#dup:   566122.7 i/s - 2.37x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#               Hash[]     8.908k i/100ms
#             Hash#dup   381.560k i/100ms
# Calculating -------------------------------------
#               Hash[]      3.784M (±11.8%) i/s -     18.466M in   4.965368s
#             Hash#dup      3.756M (± 2.5%) i/s -     19.078M in   5.082968s

# Comparison:
#               Hash[]:  3784181.8 i/s
#             Hash#dup:  3755853.6 i/s - same-ish: difference falls within error