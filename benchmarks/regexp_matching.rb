# frozen_string_literal: true

require_relative '../benchmark_helper'

def fast
  "foo".match?(/boo/)
end

def slow
  "foo" =~ /boo/
end

def slower
  /boo/ === "foo"
end

def slowest
  "foo".match(/boo/)
end

Benchmark.ips do |x|
  x.report("String#match?") { fast }
  x.report("String#=~") { slow }
  x.report("Regexp#===") { slower }
  x.report("String#match") { slowest }
  x.compare!
end

# CRuby 2.6.6
# Warming up --------------------------------------
#        String#match?   620.475k i/100ms
#            String#=~   215.877k i/100ms
#           Regexp#===   206.886k i/100ms
#         String#match   191.186k i/100ms
# Calculating -------------------------------------
#        String#match?      6.485M (± 6.7%) i/s -     32.265M in   5.001339s
#            String#=~      2.166M (± 9.4%) i/s -     10.794M in   5.030940s
#           Regexp#===      2.010M (± 8.2%) i/s -     10.137M in   5.080050s
#         String#match      1.858M (± 6.6%) i/s -      9.368M in   5.066043s

# Comparison:
#        String#match?:  6485030.7 i/s
#            String#=~:  2166374.5 i/s - 2.99x  (± 0.00) slower
#           Regexp#===:  2010285.7 i/s - 3.23x  (± 0.00) slower
#         String#match:  1857627.1 i/s - 3.49x  (± 0.00) slower


# truffleruby 21.10.0 - ruby 2.7.3
# Warming up --------------------------------------
#        String#match?    12.884k i/100ms
#            String#=~     1.172M i/100ms
#           Regexp#===     1.139M i/100ms
#         String#match     1.033M i/100ms
# Calculating -------------------------------------
#        String#match?     11.582M (± 8.4%) i/s -     56.831M in   4.962641s
#            String#=~     11.532M (± 3.5%) i/s -     58.615M in   5.089606s
#           Regexp#===     11.622M (± 3.1%) i/s -     58.104M in   5.005125s
#         String#match     11.214M (± 2.9%) i/s -     56.830M in   5.072787s

# Comparison:
#           Regexp#===: 11622207.2 i/s
#        String#match?: 11581505.9 i/s - same-ish: difference falls within error
#            String#=~: 11532027.0 i/s - same-ish: difference falls within error
#         String#match: 11214133.8 i/s - same-ish: difference falls within error
