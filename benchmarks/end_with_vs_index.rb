# frozen_string_literal: true

require_relative '../benchmark_helper'

STRING = 'wpiuhygfuoidshv09hefuihndkjfnhskjdhf987sdf0ywe'

def index
  STRING[-1] == 'e'
end

def end_with
  STRING.end_with? 'e'
end

Benchmark.ips do |x|
  x.report('end_with') { end_with }
  x.report('index') { index }
  x.compare!
end

# CRuby 2.6.8
# Comparison:
#        block to Proc:   267765.4 i/s
#         nested yield:   195107.2 i/s - 1.37x  (± 0.00) slower

# CRuby 3.1.1
# Comparison:
#        block to Proc:   238052.6 i/s
#         nested yield:   176143.8 i/s - 1.35x  (± 0.00) slower
